import WinQuoteCrawler as WQ


class API:
    @staticmethod
    def loop(data):
        if data["request"] == "get_best_products":
            return WQ.get_best_products(data)

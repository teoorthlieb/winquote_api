from bs4 import BeautifulSoup
import datetime
import requests
from pymongo import MongoClient


def check_substring(substring, string):
    if string is not None:
        return substring in string
    else:
        return False


# Get data to fetch from WinQuote
url = 'https://www.winquote.net/compete.pl'
headers = {
    'Origin': 'https://www.winquote.net',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-GB,en;q=0.9,en-US;q=0.8,fr;q=0.7,es;q=0.6',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36\
     (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,\
    image/webp,image/apng,*/*;q=0.8',
    'Cache-Control': 'max-age=0',
    'Referer': 'https://www.winquote.net/quote_life_ca.html',
    'Connection': 'keep-alive',
}


def process_crawled_data(data, n,
                         product_type,
                         payment_frequency='annual',
                         currency='CAD'):
    """
    Gather data and create JSON doc

    """
    print('process crawled_data: {}'.format(data))
    # Get the 3 cheapest products
    # from different insurers
    # best_n = data[:n]
    # import pdb; pdb.set_trace()
    best_n = []
    names = []
    i = 1
    names.append(data[0][0])
    best_n.append(data[0])
    for d in data[1:]:
        if d[0] not in names:
            best_n.append(d)
            names.append(d[0])

    best_n = best_n[:n]
    print("Best", n, ":", best_n)
    payload = []
    for policy in best_n:
        insurer_name = policy[0]
        insurer_icon = ''
        premiums = policy[2]
        payload.append({
            'insurer_name': insurer_name,
            'insurer_icon': insurer_icon,
            'payment_frequency': payment_frequency,
            'currency': currency,
            'price': premiums,
            'product': {
                'insurance_type': 'life_insurance',
                'product_type_id': product_type
            }
        })

    return payload


def crawl(month, day, year, gender,
          smoker, premium, product, term,
          payment_frequency):
    """

    :param month:
        'mm'
    :param day:
        'dd'
    :param year:
        'yyyy'
    :param gender:
        'male' or 'female'; you need to change
        it to '1': Male or '2': Female
    :param smoker:
        bool; you need to change
        it to 'N' or 'Y'
    :param premium:
        int
    :param product:
        product to buy
    :param term:
        term of None if whole life
        insurance
    :return:
    month = '01'
    day = '01'
    year = '1993'
    premium = '500000'
    product = 'term'
    term = 10
    crawl('01', '01', '1993', 'N', 'N', '475000', 'term', 10)
    """
    global headers

    if gender == 'male':
        gender = '1'
    else:
        gender = '2'

    if smoker:
        smoker = 'Y'
    else:
        smoker = 'N'
    # import pdb; pdb.set_trace()
    if payment_frequency == 'annual':
        payment_mode = '0'
    elif payment_frequency == 'monthly':
        payment_mode = '3'
    else:
        return NotImplementedError

    if product == 'term':
        data_append = find_id_term(term)
    elif product == 'critical_illness':
        data_append = find_id_ci(term)
    # elif product == 'permanent':
    #    data_append = find_id_whole_life(term)
    else:
        data_append = find_id_ci(term)
        # return NotImplementedError

    data = list([
      ('month', month),
      ('day', day),
      ('year', year),
      ('First Client Gender', gender),
      ('First Client Tobacco Use', smoker),
      ('fp_smoker_cigarette', '100'),
      ('fp_smoker_cigar', '100'),
      ('fp_smoker_pipe', '100'),
      ('fp_smoker_snuff', '100'),
      ('fp_smoker_cigarello', '100'),
      ('fp_smoker_chew', '100'),
      ('fp_smoker_marijuana', '100'),
      ('fp_smoker_prescribe', '100'),
      ('Province', '8'),
      ('First Client Premium', premium),
      ('Payment Mode', payment_mode),
      ('First Client Risk', '1'),
      ('type', 'single'),
      ('co_code', 'ca1122573208'),
      ('co_name', 'WinQuote'),
      ('report_type', 'rank'),
      ('version_code', 'ca:se:1.3'),
      ('lang_code', 'en'),
    ])

    for d in data_append:
        data.append(d)
    response = requests.post('https://www.winquote.net/compete.pl',
                             headers=headers, data=data)
    # import pdb; pdb.set_trace()

    html = response.content
    soup = BeautifulSoup(html, 'html.parser')
    table = soup.findAll('tr')
    rows = [tr for tr in table if check_substring('rtr_but', tr.get('id'))]

    data = []
    for row in rows:
        tds = row.findAll('td')
        insurer = tds[1].string
        product = tds[2].text
        price_year = tds[3].text
        price_month = tds[4].text
        data.append([insurer, product, price_year, price_month])
    return data


def find_id_term(term):
    possible_terms = [1, 10, 15, 20, 25, 30, 35, 40]
    ids = ['0{}'.format(i + 1) for i in range(len(possible_terms))]

    return [('First Client Coverage Type',
             ids[possible_terms.index(min(possible_terms,
                                          key=lambda x:abs(x - term)))]),
            ('First Client Plan Type OT', '0'),
            ('First Client Plan Type LB', '0'),
            ]


def find_id_ci(term, duo=False):
    """

    :param term:
    :param duo:
        Critical-Illness + Life insurance
        TODO: not working yet
    :return:
    """
    possible_terms = [10, 15, 20, 25, 30]
    fc_plan_type = ['0', '1', '2', '3', '4']

    if duo:
        type_ = '14'
    else:
        type_ = fc_plan_type[possible_terms.index(min(
            possible_terms, key=lambda x: abs(x - term)))]

    data = [('First Client Coverage Type', '5'),
            ('First Client Plan Type OT', '0'),
            ('First Client Plan Type LB', type_),
            ('fc_sf_00', '0'),
            ('fc_sf_01', '1'),
            ('fc_sf_34', '34')]
    return data


def find_id_whole_life(term):

    if term is None:
        return [('First Client Coverage Type', '30'),
                ('First Client Plan Type OT', '0'),
                ('First Client Plan Type LB', '0')]

    possible_terms = [15, 20, 25]
    fc_plan_type = ['34', '33', '32']
    type_ = fc_plan_type[possible_terms.index(min(
        possible_terms, key=lambda x: abs(x - term)))]
    return [('First Client Coverage Type', type_),
            ('First Client Plan Type OT', '0'),
            ('First Client Plan Type LB', '0')]


def round_up_premium(premium):
    possible_premiums = [25000 + 5000 * i for i in range(15)] + \
                        [100000 + 25000 * i for i in range(16)] + \
                        [500000 + 50000 * i for i in range(10)] + \
                        [1000000 + 250000 * i for i in range(8)] + \
                        [3000000 + 500000 * i for i in range(4)]
    return min(possible_premiums, key=lambda x: abs(x - premium))


def get_best_products(data):
    winquote_data = crawl(day='01', month='01', year=data["year"], gender=data["gender"],
          smoker=data["smoker"], premium=round_up_premium(int(data["premium"])), product=data["product"],
          term=data["term"], payment_frequency='monthly')

    return process_crawled_data(winquote_data, n=3, product_type=data["product"],
                                payment_frequency='monthly', currency='CAD')


if __name__ == '__main__':

    tt = get_best_products('testing_crawler')
    print(tt)

from flask import Flask, request, jsonify
from flask_cors import cross_origin
from api import API

app = Flask(__name__)


@app.route('/', methods=['GET'])
def verify():
    # Webhook verification
    return "WinQuote API is up!", 200


@cross_origin(supports_credentials=True)
@app.route('/', methods=['POST', 'OPTIONS'])
def webhook():
    data = request.get_json(force=True)
    url = request.base_url
    print(data)
    print(url)

    # the answer to the user's question is the
    # response, to allow cross domain sharing
    # we need to modify the header
    # resp = Response(response='hellooeoe')
    # resp.headers['Access-Control-Allow-Origin'] = 'http://127.0.0.1:5000/'
    try:
        resp = API.loop(data)
    except KeyError as e:
        resp = list([("err", str(e))])
    # answer, quick_replies = resp
    # print('answer: %s' % answer)
    # print('quick_replies: %s' % str(quick_replies))
    # return jsonify({'text': answer,
    #                'quick_replies': quick_replies})
    print("Resp: ", resp)
    rep = jsonify(resp)
    print("JSONifyied", rep.data)
    return rep


if __name__ == '__main__':
    app.run(debug=True, port=5001)
